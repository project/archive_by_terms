<?php


/**
 * hook_views_default_views
 * 
 * automatically import the needed view
 */
function archive_by_terms_views_default_views() {
  $view = new view;
  $view->name = 'archive_by_year';
  $view->description = '';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = '0';
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Standards', 'default');
  $handler->override_option('sorts', array (
    'created' => array (
      'id' => 'created',
      'table' => 'node',
      'field' => 'created',
      'order' => 'DESC',
      'granularity' => 'second',
      'relationship' => 'none',

      
    ),

    
  ));
  $handler->override_option('arguments', array (
    'tid' => array (
      'default_action' => 'ignore',
      'style_plugin' => 'default_summary',
      'style_options' => array (
        'count' => TRUE,
        'override' => FALSE,
        'items_per_page' => 25,

        
      ),
      'wildcard' => 'all',
      'wildcard_substitution' => 'Alle',
      'title' => '',
      'default_argument_type' => 'fixed',
      'default_argument' => '',
      'validate_type' => 'none',
      'validate_fail' => 'not found',
      'break_phrase' => 0,
      'add_table' => 0,
      'require_value' => 0,
      'reduce_duplicates' => 0,
      'set_breadcrumb' => 0,
      'id' => 'tid',
      'table' => 'term_node',
      'field' => 'tid',
      'relationship' => 'none',
      'default_argument_user' => 0,
      'default_argument_fixed' => '',
      'default_argument_php' => '',
      'validate_argument_node_type' => array (
        'page' => 0,
        'story' => 0,

        
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array (
        '1' => 0,

        
      ),
      'validate_argument_type' => 'tid',
      'validate_argument_php' => '',

      
    ),
    'created_year' => array (
      'id' => 'created_year',
      'table' => 'node',
      'field' => 'created_year',
      'default_action' => 'ignore',
      'style_plugin' => 'default_summary',
      'style_options' => array (
        'count' => TRUE,
        'override' => FALSE,
        'items_per_page' => 25,

        
      ),
      'wildcard' => 'all',
      'wildcard_substitution' => 'Alle',
      'title' => '%1 - %2',
      'default_argument_type' => 'fixed',
      'default_argument' => '',
      'validate_type' => 'none',
      'validate_fail' => 'not found',
      'relationship' => 'none',
      'default_argument_fixed' => '',
      'default_argument_php' => '',
      'validate_argument_node_type' => array (
        'page' => 0,
        'story' => 0,

        
      ),
      'validate_argument_php' => '',
      'default_argument_user' => 0,
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array (
        '1' => 0,

        
      ),
      'validate_argument_type' => 'tid',

      
    ),

    
  ));
  $handler->override_option('access', array (
    'type' => 'none',
    'role' => array (),
    'perm' => '',

    
  ));
  $handler->override_option('row_plugin', 'node');
  $handler->override_option('row_options', array (
    'teaser' => TRUE,
    'links' => TRUE,

    
  ));
  $handler = $view->new_display('page', 'Seite', 'page');
  $handler->override_option('path', 'archive/%/%');
  $handler->override_option('menu', array (
    'type' => 'none',
    'title' => '',
    'weight' => 0,

    
  ));
  $handler->override_option('tab_options', array (
    'type' => 'none',
    'title' => '',
    'weight' => 0,

    
  ));

  // Add view to list of views to provide.
  $views[$view->name] = $view;

  // At the end, return array of default views.
  return $views;

}